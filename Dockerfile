#base image
FROM node:10-alpine AS builder

#set working directory 

WORKDIR /usr/src/app

#copy all files from current directory to docker
COPY . /usr/src/app



#add '/usr/src/app.node_modules/.bin' to $PATH


#install and cache app dependencies
RUN yarn
#multi stage build
FROM node:10-alpine as noderun
WORKDIR /app
COPY --from=builder /usr/src/app/ ./
COPY package*.json ./
RUN yarn 

#start app
CMD ["npm","start"]